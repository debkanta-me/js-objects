function defaults(obj, defaultProps) {
    for (const key in obj) {
        if (typeof obj[key] == "undefined") {
            obj[key] = defaultProps[key];
        }
    }

    return typeof obj != "undefined" ? obj : {};
}

module.exports = defaults;
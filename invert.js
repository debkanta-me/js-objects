function invert(obj){
    let copyObj = {}
    for(const key in obj){
      copyObj[obj[key]] = key;
    }
  
    return copyObj;
  }

  module.exports = invert;
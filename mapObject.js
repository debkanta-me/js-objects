function mapObject(obj, cb){
    if(typeof obj != "object") return {};
    if(typeof cb != "function") return obj;
    for(const key in obj){
        obj[key] = cb(obj[key]);
    }
    return obj;
}

module.exports = mapObject;
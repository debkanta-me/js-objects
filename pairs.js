function pairs(obj){
    let pairs = []

    for(const key in obj){
        let pair = [];
        pair.push(key);
        pair.push(obj[key]);
        pairs.push(pair);
    }

    return pairs;
}

module.exports = pairs;
const defaults = require("../defaults");

const testObject = { name: 'Bruce Wayne', age: 36, location: undefined };

console.log(defaults(testObject, {location: "Manchester"}));
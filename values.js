function values(obj){
    let values = []

    for(const key in obj){
        values.push(obj[key]);
    }

    return values;
}

module.exports = values;